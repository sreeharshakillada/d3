import React, { useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

interface Styles {
    [key: string]: React.CSSProperties;
}

interface DataPoint {
    value: number;
    label: string;
}

const styles: Styles = {
    topHalf: {
        minHeight: '50vh',
        backgroundColor: '#1a1a1a',
        color: '#fff',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Arial, sans-serif',
        width: '100%',
        boxSizing: 'border-box',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    contentContainer: {
        width: '50%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    chartContainer: {
        width: '100%',
    },
    header: {
        fontFamily: 'Lobster, cursive',
        fontSize: '2em',
        marginBottom: '20px',
    },
    rowLayout: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
        marginTop: '20px',
    },
    column: {
        flex: 1,
        padding: '10px',
    },
    columnContent: {
        backgroundColor: '#333',
        padding: '20px',
        borderRadius: '10px',
    },
    description: {
        marginTop: '20px',
        textAlign: 'center',
        fontStyle: 'italic',
        color: 'lightgray',
    }
};

const MainPage: React.FC = () => {
    const chartContainerRef = useRef<HTMLDivElement | null>(null);
    const [data, setData] = useState<DataPoint[]>([
        { value: 15, label: 'A' },
        { value: 25, label: 'B' },
        { value: 15, label: 'C' },
        { value: 25, label: 'D' },
        { value: 15, label: 'E' },
        { value: 25, label: 'F' },
        { value: 15, label: 'G' },
        { value: 25, label: 'H' },
        { value: 15, label: 'I' },
        { value: 25, label: 'J' }
    ]);

    useEffect(() => {
        const width = chartContainerRef.current?.clientWidth || 250;
        const height = chartContainerRef.current?.clientHeight || 250;
        const radius = Math.min(width, height) / 2;

        // const colorScale = d3.scaleOrdinal(d3.schemeCategory10);
        const colorScale = d3.scaleOrdinal()
            .domain(data.map(d => d.label))
            .range(['#ADD8E6', '#20b2aa', '#b0c4de', '#FFFFED']);

        const pie = d3.pie<DataPoint>().value(d => d.value);

        const arc = d3.arc<any, d3.DefaultArcObject>()
            .outerRadius(radius - 10)
            .innerRadius(0);

        // Remove existing chart
        d3.select(chartContainerRef.current).selectAll("*").remove();

        const svg = d3.select(chartContainerRef.current)
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", `translate(${width / 2},${height / 2})`);

        const arcs = svg.selectAll("arc")
            .data(pie(data) as d3.PieArcDatum<DataPoint>[])
            .enter()
            .append("g")
            .attr("class", "arc")
            .on("mouseover", handleMouseOver)
            .on("mouseout", handleMouseOut);

        updateColors(); // Initial call to start the animation

        function updateColors() {
            arcs.select("path")
                .transition()
                .duration(1000) // Duration of each transition in milliseconds
                .attr("fill", () => getRandomColor()); // Use a function to get a random color
        }

        setInterval(() => {
            updateColors();
        }, 2000); // Interval between color updates in milliseconds

        function getRandomColor() {
            // Generate a random hex color code
            return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
        }

        arcs.append("path")
            .attr("d", arc as any)
            .attr("fill", (d: d3.PieArcDatum<DataPoint>, i: number) => colorScale(d.data.label) as string);


        arcs.append("text")
            .attr("transform", (d: any) => `translate(${arc.centroid(d)})`)
            .attr("text-anchor", "middle")
            .style("font-size", "12px")
            .style("fill", (d: any) => getContrastColor(colorScale(d.data.label) as string))
            .text((d: any) => d.data.label);

        function getContrastColor(hexColor: string): string {
            // Determine if the background color is light or dark and set the text color accordingly
            const r = parseInt(hexColor.slice(1, 3), 16);
            const g = parseInt(hexColor.slice(3, 5), 16);
            const b = parseInt(hexColor.slice(5, 7), 16);
            const brightness = (r * 299 + g * 587 + b * 114) / 1000;
            return brightness > 128 ? "black" : "white";
        }


        function handleMouseOver(this: any, d: d3.PieArcDatum<DataPoint>) {
            d3.select(this)
                .transition()
                .duration(200)
                .attr("transform", (d: any) => {
                    const angle = (d.startAngle + d.endAngle) / 2;
                    const distance = 10;
                    const x = Math.sin(angle) * distance;
                    const y = -Math.cos(angle) * distance;
                    return `translate(${x},${y})`;
                })
                .attr("fill", "lightgray"); // Set the color on hover
        }

        function handleMouseOut(this: any) {
            d3.select(this)
                .transition()
                .duration(200)
                .attr("transform", "translate(0,0)")
                .attr("fill", (d: any) => colorScale(d.data.label) as string);

        }

    }, [data]);

    return (
        <div style={styles.topHalf}>
            <div style={styles.contentContainer}>
                <h1 style={styles.header}>AHEX Tech. | d3.js</h1>
                <div style={styles.chartContainer} ref={chartContainerRef}></div>
                <p style={styles.description}>
                    AHEX Tech leverages the power of D3.js in its projects to create dynamic and interactive data visualizations.
                    Through the seamless integration of D3.js, AHEX Tech ensures that data comes to life,
                    offering a compelling visual narrative to users.
                    This implementation enhances the user experience by presenting complex information in an intuitive and engaging manner,
                    making AHEX Tech's projects both informative and visually impactful.
                </p>
            </div>
            <div style={styles.rowLayout}>
                <div style={styles.column}>
                    <div style={styles.columnContent}>
                        {/* Content for the first column */}
                        <h2>Column 1</h2>
                        <p>Some text or components...</p>
                    </div>
                </div>
                <div style={styles.column}>
                    <div style={styles.columnContent}>
                        {/* Content for the second column */}
                        <h2>Column 2</h2>
                        <p>Some text or components...</p>
                    </div>
                </div>
                <div style={styles.column}>
                    <div style={styles.columnContent}>
                        {/* Content for the third column */}
                        <h2>Column 3</h2>
                        <p>Some text or components...</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MainPage;
